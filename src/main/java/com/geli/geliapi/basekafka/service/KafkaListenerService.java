package com.geli.geliapi.basekafka.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.geli.geliapi.basekafka.dto.EmailNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class KafkaListenerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Autowired private KafkaSenderService kafkaSenderService;
    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.notifikasi.request}")
    public void handleNotifikasi(String msg) {
        LOGGER.debug("Terima message : {}", msg);
        try {
            EmailNotification emailNotification
                    = objectMapper.readValue(msg, EmailNotification.class);

            LOGGER.debug("Email Notification : {}", emailNotification);

            kafkaSenderService.kirimStatus(UUID.randomUUID().toString(), "success");
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

}
