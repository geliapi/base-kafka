package com.geli.geliapi.basekafka.dto;

import lombok.Data;

@Data
public class EmailNotification {

    private String to;
    private String from;
    private String subject;
    private String content;

}
